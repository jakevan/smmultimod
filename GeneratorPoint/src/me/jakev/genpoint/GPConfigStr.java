package me.jakev.genpoint;

import api.mod.config.FileConfiguration;

import java.util.Locale;

/**
 * Created by Jake on 10/23/2021.
 * <insert description here>
 */
public enum GPConfigStr {
    BROADCAST_MESSAGE("."),
    NAMED_INVENTORY_INSERT("mainstor"),
    ;

    private String defaultVal;
    private String value;
    GPConfigStr(String defaultVal) {
        this.defaultVal = defaultVal;
    }
    public String getValue(){
        return value;
    }
    public static void init(GenPoint gp){
        FileConfiguration config = gp.getConfig("config");
        for (GPConfigStr value : GPConfigStr.values()) {
            value.value = config.getConfigurableValue(value.getConfigName(), value.defaultVal);
        }
        config.saveConfig();
    }
    public String getConfigName(){
        return name().toLowerCase(Locale.ENGLISH);
    }
}
