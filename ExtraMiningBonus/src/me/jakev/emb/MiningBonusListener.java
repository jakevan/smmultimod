package me.jakev.emb;

import api.listener.fastevents.StatusEffectApplyListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import javax.vecmath.Vector3f;
import java.lang.reflect.Field;

/**
 * Created by Jake on 10/30/2021.
 * <insert description here>
 */
public class MiningBonusListener implements StatusEffectApplyListener {
    public static void main(String[] args) {
        for (int i = 0; i < 2000; i++) {
            System.out.println(i*100);
            System.out.println(getMiningBonus(i * 100));
        }
    }
    public static int getMiningBonus(int count){
        return count/ExtraMB.div;
    }
    @Override
    public int apply(ConfigEntityManager conf, StatusEffectType type, int i) {
        if(type == StatusEffectType.MINING_BONUS_ACTIVE){
            try {
                Field eid = ConfigEntityManager.class.getDeclaredField("entityId");
                eid.setAccessible(true);
                long o = (long) eid.get(conf);
                if(GameServerState.instance != null){
                    Sendable dss = GameServerState.instance.getLocalAndRemoteObjectContainer().getDbObjects().get(o);
                    if(dss instanceof SegmentController){
                        int miningBonus = getMiningBonus(((SegmentController) dss).getElementClassCountMap().get(ExtraMB.block.id));
                        return miningBonus;
                    }
                }
                if(GameClientState.instance != null){
                    Sendable dss = GameClientState.instance.getLocalAndRemoteObjectContainer().getDbObjects().get(o);
                    if(dss instanceof SegmentController){
                        return getMiningBonus(((SegmentController) dss).getElementClassCountMap().get(ExtraMB.block.id));
                    }
                }


            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return i;
    }

    @Override
    public double apply(ConfigEntityManager configEntityManager, StatusEffectType statusEffectType, double v) {
        return v;
    }

    @Override
    public float apply(ConfigEntityManager configEntityManager, StatusEffectType statusEffectType, float v) {
        return v;
    }

    @Override
    public boolean apply(ConfigEntityManager configEntityManager, StatusEffectType statusEffectType, boolean b) {
        return b;
    }

    @Override
    public Vector3f apply(ConfigEntityManager configEntityManager, StatusEffectType statusEffectType, Vector3f vector3f) {
        return vector3f;
    }
}
