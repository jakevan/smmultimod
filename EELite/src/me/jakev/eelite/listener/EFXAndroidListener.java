package me.jakev.eelite.listener;

import api.common.GameClient;
import api.listener.Listener;
import api.listener.events.player.MailReceiveEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.sound.AudioUtils;
import me.jakev.eelite.EFXLite;

/**
 * Created by Jake on 10/29/2022.
 * <insert description here>
 */
public class EFXAndroidListener {
    public static void init(StarMod inst){
        // The unique sound name. make sure to prefix the name with your mod to avoid conflicts
        final String soundName = "efxlite_androidnotif";
        // Register the mod sound
        AudioUtils.registerModSound(inst.getSkeleton(), soundName, EFXLite.class.getResourceAsStream("notif.ogg"));
        final long registerTime = System.currentTimeMillis();
        // Add a listener on mail receive event to play our sound.
        StarLoader.registerListener(MailReceiveEvent.class, inst, new Listener<MailReceiveEvent>() {
            @Override
            public void onEvent(MailReceiveEvent ev) {
                // Make sure the context is the client only
                if(ev.isServer()){
                    return;
                }
                if(Math.random() < 0.8){
                    return;
                }
                if(Math.abs(System.currentTimeMillis() - registerTime) > 1000*60L){
                    //10m difference only
                    if (GameClient.getClientState() != null) {
                        AudioUtils.clientPlaySound(soundName, 1f, 1f);
                    }
                }
            }
        });
    }
}
