package me.jakev.bigstars;

import api.listener.fastevents.FastListenerCommon;
import api.listener.fastevents.FrameBufferDrawListener;
import api.mod.StarMod;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;

import javax.vecmath.Vector3f;

public class BigStars extends StarMod {
    @Override
    public void onEnable() {
        FastListenerCommon.frameBufferDrawListeners.add(new FrameBufferDrawListener(){
            @Override
            public void preGodRaysDraw(MainGameGraphics g) {
                FrameBufferDrawListener.drawSphereForGodRays = false;
                GameClientState state = g.getState();
                Vector3f secondSunLightPos = state.getWorldDrawer().secondSunLightPos;


                state.getScene().getMainLight().draw();
                Vector3f lPos = AbstractScene.mainLight.getPos();

                Vector3i relSysPos = Galaxy.getLocalCoordinatesFromSystem(state.getPlayer().getCurrentSystem(), new Vector3i());

                int systemType = state.getCurrentGalaxy().getSystemType(relSysPos);
                if (Galaxy.USE_GALAXY && (systemType == Galaxy.TYPE_BLACK_HOLE || state.getCurrentGalaxy().isVoid(relSysPos))) {
                    return;
                }
                if (state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive()) {
                    return;
                }
                {
                    GlUtil.glPushMatrix();
                    //		sunDrawer.draw();
                    GlUtil.translateModelview(lPos.x, lPos.y, lPos.z);

                    float max = 30000;
                    float smallFac = 1;
                    float normalSize = systemType == Galaxy.TYPE_GIANT ? 9 : 4;
                    float d = (float) Math.pow(lPos.length(), 1.3f);
                    if (d < 30000) {
                        smallFac = d / 30000f;
                        normalSize *= smallFac;
                    }

                    normalSize *= 10;

                    Controller.getResLoader().getMesh("Sphere").setScale(normalSize, normalSize, normalSize);
                    Controller.getResLoader().getMesh("Sphere").draw();
                    GlUtil.glPopMatrix();
                }

                if (secondSunLightPos != null) {
                    GlUtil.glPushMatrix();
                    //		sunDrawer.draw();
                    GlUtil.translateModelview(secondSunLightPos.x, secondSunLightPos.y, secondSunLightPos.z);

                    float max = 30000;
                    float smallFac = 1;
                    float normalSize = 4;
                    float d = (float) Math.pow(lPos.length(), 1.3f);
                    if (d < 30000) {
                        smallFac = d / 30000f;
                        normalSize *= smallFac;
                    }

                    Controller.getResLoader().getMesh("Sphere").setScale(normalSize, normalSize, normalSize);
                    Controller.getResLoader().getMesh("Sphere").draw();
                    GlUtil.glPopMatrix();
                }
            }
        });
    }
}
