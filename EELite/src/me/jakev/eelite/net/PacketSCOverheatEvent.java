package me.jakev.eelite.net;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.particle.ModParticleUtil;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.particles.shipexplode.ColorFlashParticle;
import me.jakev.eelite.particles.shipexplode.ShipEmitterTriggerParticle;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;

/**
 * Created by Jake on 1/29/2022.
 * <insert description here>
 */
public class PacketSCOverheatEvent extends Packet {
    int sec;
    Vector3f pos;
    boolean killed;
    float radius;

    public PacketSCOverheatEvent(int sec, Vector3f pos, boolean killed, float radius) {
        this.sec = sec;
        this.pos = pos;
        this.killed = killed;
        this.radius = radius;
    }

    public PacketSCOverheatEvent() {
    }

    @Override
    public void readPacketData(PacketReadBuffer buf) throws IOException {
        sec = buf.readInt();
        pos = buf.readVector3f();
        killed = buf.readBoolean();
        radius = buf.readFloat();
    }

    @Override
    public void writePacketData(PacketWriteBuffer buf) throws IOException {
        buf.writeInt(sec);
        buf.writeVector3f(pos);
        buf.writeBoolean(killed);
        buf.writeFloat(radius);
    }

    @Override
    public void processPacketOnClient() {
        if(killed){
            ShipEmitterTriggerParticle particle = new ShipEmitterTriggerParticle();
            ModParticleUtil.playClient(sec, pos, EFXSprites.NOTHING2.getSprite(), particle);
        }else{
            ColorFlashParticle cf = new ColorFlashParticle(radius, new Vector4f(1, 0, 0, 1), new Vector4f(1, 0, 0, 1));
            cf.lifetimeMs = 1000;
            ModParticleUtil.playClient(sec, pos, EFXSprites.FLASH.getSprite(), cf);
        }
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {

    }
}
