package me.jakev.uploadmycrashes;

import api.common.GameCommon;
import api.mod.StarMod;
import org.schema.common.LogUtil;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Created by Jake on 10/20/2022.
 * <insert description here>
 */
public class UpMain extends StarMod {
    @Override
    public void onEnable() {
        if(GameCommon.isDedicatedServer()){
            System.err.println("[UpMain] Dedicated server, starting server log handler on port 5252");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    new LogServer();
                }
            }).start();

        }else if(GameCommon.isClientConnectedToServer()){

            System.err.println("[UpMain] Client connected to server, adding logging close handler");
            Field logger = null;
            try {
                logger = LogUtil.class.getDeclaredField("logger");
                logger.setAccessible(true);
                Logger mLogger = (Logger) logger.get(null);

                mLogger.addHandler(new LogHandler());

            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
