package me.jakev.eelite.particles.cb;

import api.utils.particle.ModParticle;
import api.utils.particle.TextureSheetMergeAlgorithm;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class CircleForwardParticle extends ModParticle {
    public Vector4f startColor = new Vector4f(1,1,1,1);
    public Vector4f endColor = new Vector4f(1,1,1,0);
    private final float startSize;
    private final float endSize;
    private int randomLife;
    private Matrix3f dirM = new Matrix3f();
    public CircleForwardParticle(float startSize, float endSize, Vector3f dir) {
        this.velocity.set(dir);
        this.startSize = startSize;
        this.endSize = endSize;
    }


    @Override
    public void update(long currentTime) {
        colorOverTime(this, currentTime, startColor, endColor);
        sizeOverTime(this, currentTime, startSize, endSize);
    }
}
