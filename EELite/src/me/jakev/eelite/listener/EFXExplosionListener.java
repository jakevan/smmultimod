package me.jakev.eelite.listener;

import api.listener.Listener;
import api.listener.events.entity.SegmentControllerOverheatEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticleUtil;
import com.bulletphysics.linearmath.Transform;
import me.jakev.eelite.net.PacketSCOverheatEvent;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;

/**
 * Created by Jake on 1/29/2022.
 * <insert description here>
 */
public class EFXExplosionListener {
    public static void init(final StarMod inst) {
        StarLoader.registerListener(SegmentControllerOverheatEvent.class, new Listener<SegmentControllerOverheatEvent>() {
            @Override
            public void onEvent(final SegmentControllerOverheatEvent event) {
                final Vector3f pos = event.getEntity().getWorldTransformCenterOfMass(new Transform()).origin;
                final int sectorId = event.getEntity().getSectorId();
                float radius = event.getEntity().getBoundingSphereTotal().radius;
                PacketSCOverheatEvent packet = new PacketSCOverheatEvent(sectorId, pos, event.isKilled(), radius);
                for (PlayerState p : ModParticleUtil.getPlayersInRange(sectorId)) {
                    PacketUtil.sendPacket(p, packet);
                }
            }
        }, inst);

    }
}
