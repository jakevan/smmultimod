package me.jakev.eelite.listener;

import api.common.GameCommon;
import api.listener.Listener;
import api.listener.events.weapon.CannonProjectileAddEvent;
import api.listener.fastevents.CannonChargeListener;
import api.listener.fastevents.FastListenerCommon;
import api.mod.StarLoader;
import api.utils.particle.ModParticleUtil;
import api.utils.particle.TextureSheetMergeAlgorithm;
import api.utils.particle.utils.ShipRelativeParticle;
import com.bulletphysics.linearmath.Transform;
import me.jakev.eelite.EFXLite;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.particles.*;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponUnit;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.container.TransformTimed;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Random;


/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class EFXCannonListener {
    private static Matrix3f[] flareMatricies = new Matrix3f[2];
    public static Matrix3f createViewMatrix(Vector3f forward) {
        Vector3f up = new Vector3f(0, 1, 0);
        Vector3f right;
        // handle the case where forward and up are equal
        if (forward.equals(up)) {
            right = new Vector3f(1, 0, 0);
            up = new Vector3f(0, 0, 1);
        } else {
            right = new Vector3f();
            right.cross(forward, up);
            up.cross(right, forward);
        }
        Matrix3f m = new Matrix3f();
        m.setColumn(0, forward);
        m.setColumn(1, up);
        m.setColumn(2, right);
        return m;
    }

    public static Matrix3f createViewMatrix(SegmentController sc) {
        Matrix4f m = sc.getWorldTransformOnClient().getMatrix(new Matrix4f());
        Matrix3f m3f = new Matrix3f();
        m.getRotationScale(m3f);
        return m3f;
    }

    public static void init(EFXLite mod) {
        for (int i = 0; i < 2; i++) {
            Matrix3f normal = new Matrix3f();
            // Set forward column to... forward
            normal.setColumn(1, new Vector3f(0,0,1));
            // set up column
            Vector3f col1 = null;
            switch (i) {
                case 0:
                    col1 = new Vector3f(0,1,0);
                    break;
                case 1:
                    col1 = new Vector3f(1,0,0);
                    break;
            }
            normal.setColumn(2, col1);
            col1.cross(col1, new Vector3f(0,0,1));
            normal.setColumn(0, col1);

            flareMatricies[i] = normal;
        }
        FastListenerCommon.cannonChargeListeners.add(new CannonChargeListener() {
            @Override
            public WeaponChargeListenerReturnObject cannonHeldDown(WeaponCollectionManager weaponCollectionManager, ControllerStateInterface controllerStateInterface, Timer timer) {

                return null;
            }

            float helper = 0;

            @Override
            public WeaponChargeListenerReturnObject chargeAdded(WeaponCollectionManager cm, ControllerStateInterface controllerStateInterface, Timer timer, float v) {
                for (WeaponUnit wu : cm.getElementCollections()) {
                    float chargeSize = (wu.size()/3000F)+3F;
                    float ringSize = (wu.size()/3000F)+10F;
                    int pCount = (wu.size()/5000)+1;

                    if (!wu.getSegmentController().isOnServer()) {
                        for (int i = 0; i < pCount; i++) {
                            ChargeParticle p = new ChargeParticle(wu.getSegmentController(),
                                    new Vector3f(wu.getOutput().x - SegmentData.SEG_HALF,
                                            wu.getOutput().y - SegmentData.SEG_HALF,
                                            wu.getOutput().z - SegmentData.SEG_HALF + 0.51f), chargeSize);
                            ModParticleUtil.playClient(wu.getSegmentController().getSectorId(), new Vector3f(), EFXSprites.FLASH.getSprite(), p);
                        }



                        if (v >= cm.getWeaponChargeParams().damageChargeMax) {
                            helper += 0.00001;
                            ShipRelativeRing p2 = new ShipRelativeRing(wu.getSegmentController(),
                                    new Vector3f(wu.getOutput().x - SegmentData.SEG_HALF,
                                            wu.getOutput().y - SegmentData.SEG_HALF,
                                            wu.getOutput().z - SegmentData.SEG_HALF + 0.5f + (helper % 0.001f)), ringSize, 0);
                            p2.lifetimeMs = 220;

                            Vector3f fwd = new Vector3f();
                            wu.getSegmentController().getWorldTransformOnClient().basis.getColumn(2, fwd);

                            p2.relativeNormalOverride = createViewMatrix(new Vector3f(0, 0, 1));
//                            p2.setArbitraryNormalOverride(fwd);

                            ModParticleUtil.playClient(wu.getSegmentController().getSectorId(), new Vector3f(), EFXSprites.RING1.getSprite(), p2);
                        }
                    }

                }
                return null;
            }
        });


        StarLoader.registerListener(CannonProjectileAddEvent.class, new Listener<CannonProjectileAddEvent>() {
            private Vector3f vTmp = new Vector3f();
            @Override
            public void onEvent(CannonProjectileAddEvent event) {
                if (event.isServer()) {
                    return;
                }

                Sendable ship = GameClientState.instance.getLocalAndRemoteObjectContainer().getLocalObjects().get(event.getContainer().getOwnerId(event.getIndex()));
                short connectedType = 0;
                if (ship instanceof Ship) {
                    long weaponId = event.getContainer().getWeaponId(event.getIndex());
                    ShipManagerContainer seg = ((Ship) ship).getManagerContainer();
                    WeaponCollectionManager wcm = seg.getWeapon().getCollectionManagersMap().get(weaponId);
                    connectedType = (short) ElementCollection.getType(wcm.getSlaveConnectedElement());

                    // Go back to world space
                    Vector3f pos = new Vector3f();
                    event.getContainer().getPos(event.getIndex(), pos);

                    TransformTimed shipTrnsTmp = seg.getSegmentController().getWorldTransformOnClient();
                    Transform shipTrns = new Transform();
                    shipTrns.set(shipTrnsTmp);

                    shipTrns.inverse();
                    shipTrns.transform(pos);
                    // ====================================== Rings

                    Vector3f originPos = new Vector3f();
                    originPos.set(pos);
                    originPos.add(new Vector3f(0, 0, 0.51f));

                    int RINGS = 3;
                    if (connectedType == 6) {
                        RINGS = 1;
                    }
                    for (int i = 0; i < RINGS; i++) {
                        ShipRelativeRing p2 = new ShipRelativeRing(seg.getSegmentController(), originPos, 0, 5F - i);
                        p2.useColor = false;
                        p2.lifetimeMs = 250;

                        p2.relativeNormalOverride = createViewMatrix(new Vector3f(0, 0, 1));
                        p2.normalOverride = new Matrix3f();

                        ModParticleUtil.playClient(seg.getSegmentController().getSectorId(), new Vector3f(), EFXSprites.RING1.getSprite(), p2);

                        originPos.add(new Vector3f(0, 0, 1f));
                    }

                    // =========================================== flare
                    float scale = 6;
                    originPos.set(pos);
                    originPos.add(new Vector3f(0, 0, scale / 2f));
                    for (int i = 0; i < 2; i++) {
                        SharpFlareParticle particle = new SharpFlareParticle(seg.getSegmentController(), originPos, new Vector3f(), flareMatricies[i]);
                        particle.lifetimeMs = 50;
                        particle.sizeX = 2F;
                        particle.sizeY = scale;

                        if (connectedType != 6) {
                            ModParticleUtil.playClient(seg.getSegmentController().getSectorId(), new Vector3f(), EFXSprites.FLARE.getSprite(), particle);
                        }
                    }

                    // CB
//                    System.err.println("[EFX] Connected type: " + connectedType);
                    if(connectedType == 414 || connectedType == -32768){
                        int sec = seg.getSegmentController().getSectorId();
//                        CannonTrackingParticle cannonTrackingParticle = new CannonTrackingParticle(event.getIndex(), event.getContainer(), sec);
//                        event.getContainer().getColor(event.getIndex(), cannonTrackingParticle.color);
//                        ModParticleUtil.playClient(sec, event.getContainer().getPos(event.getIndex(), new Vector3f()), EFXSprites.NOTHING2.getSprite(), cannonTrackingParticle);
                        Vector3f startPoint = event.getContainer().getPos(event.getIndex(), new Vector3f());
                        Vector3f dir = event.getContainer().getVelocity(event.getIndex(), new Vector3f());
                        dir.normalize();
                        dir.scale(0.4F);
                        Vector4f color = event.getContainer().getColor(event.getIndex(), new Vector4f());

                        for (int i = 0; i < 600; i++) {
                            startPoint.add(dir);
                            FadeScaleParticle p = new FadeScaleParticle(0.9F, 1.0F, 0);

                            vTmp.set(startPoint);
                            p.startColor.set(color);
                            p.endColor.set(color);
                            p.startColor.w = 0.9f;
                            p.endColor.w = 0.0f;
                            p.lifetimeMs = (int) (1500+(i/50f));
                            vTmp.add(getRandomVec(0.1F));

                            ModParticleUtil.playClient(sec, vTmp, EFXSprites.MSPARK.getSprite(), p);
                        }
                    }

                }
/*
                final Vector3f dir = new Vector3f();
                Vector3f pos = new Vector3f();
                int ownerId = event.getContainer().getOwnerId(event.getIndex());
                if (!(GameCommon.getGameObject(ownerId) instanceof SegmentController)) {
                    return;
                }
                int SCALE = 20;
                event.getContainer().getPos(event.getIndex(), pos);
                event.getContainer().getVelocity(event.getIndex(), dir);
                Vector4f color = new Vector4f();
                event.getContainer().getColor(event.getIndex(), color);



                Vector3f originPos = new Vector3f(pos);

                dir.normalize();

                Vector3f offset = new Vector3f(dir);
                offset.scale(SCALE / 2F);
                pos.add(offset);

                Vector3f normalA = new Vector3f(1, 0, 0);
                Vector3f normalB = new Vector3f(0, 1, 0);
                normalA.cross(normalA, dir);
                normalB.cross(normalB, dir);

                Matrix3f w = new Matrix3f();
                Vector3f normalOvr;
                if (normalA.dot(dir) < normalB.dot(dir)) {
                    normalOvr = normalA;
                } else {
                    normalOvr = normalB;
                }

                dir.normalize();
                w.setColumn(1, dir);
                normalOvr.normalize();
                w.setColumn(2, normalOvr);

                //completely useless cause forward is unused
                Vector3f fwd = new Vector3f();
                fwd.cross(normalOvr, dir);
                fwd.normalize();
                w.setColumn(0, fwd);

                Identifiable owner = GameClientState.instance.getLocalAndRemoteObjectContainer().getLocalObjects().get(ownerId);
                if (owner instanceof Damager) {
                    int sectorId = ((Damager) owner).getSectorId();
                    for (int i = 0; i < 2; i++) {
                        SharpFlareParticle particle = new SharpFlareParticle();
                        particle.lifetimeMs = 50;
                        particle.sizeX = 2F;
                        particle.sizeY = SCALE;
                        particle.normalOverride = w;
                        if (i == 0) {
                            Matrix3f m = new Matrix3f(w);
                            m.setColumn(2, fwd);
                            particle.normalOverride = m;
                        }
                        if (connectedType != 6) {
                            ModParticleUtil.playClient(sectorId, pos, EFXSprites.FLARE.getSprite(), particle);
                        }
                    }
                    dir.scale(0.5F);
                    originPos.add(dir);
                    dir.scale(10F);
                    int RINGS = 3;
                    if (connectedType == 6) {
                        RINGS = 1;
                    }
                    for (int i = 0; i < RINGS; i++) {
                        ExpandingRingParticle particle = new ExpandingRingParticle(0, 5F - i);
                        particle.lifetimeMs = 300;

                        particle.setArbitraryNormalOverride(dir);


                        ModParticleUtil.playClient(sectorId, originPos, EFXSprites.RING1.getSprite(), particle);
                        originPos.add(dir);
                    }

                }*/

            }
        }, mod);
    }
    static Vector3f v3f = new Vector3f();
    public static Vector3f getRandomVec(float size){
        v3f.set(TextureSheetMergeAlgorithm.randFloat(size),TextureSheetMergeAlgorithm.randFloat(size),TextureSheetMergeAlgorithm.randFloat(size));
        return v3f;
    }
}
