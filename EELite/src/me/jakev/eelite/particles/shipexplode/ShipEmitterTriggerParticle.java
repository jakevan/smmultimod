package me.jakev.eelite.particles.shipexplode;

import api.utils.StarRunnable;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import api.utils.particle.TextureSheetMergeAlgorithm;
import me.jakev.eelite.EFXLite;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.particles.FadeScaleParticle;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

/**
 * Created by Jake on 1/3/2021.
 * <insert description here>
 */
public class ShipEmitterTriggerParticle extends ModParticle {
    boolean ran = false;
    @Override
    public void update(long currentTime) {
        if(!ran) {
            final Vector3f pos = this.position;
            ran = true;

            FlashFieldEmitterParticle particle = new FlashFieldEmitterParticle(60);
            particle.lifetimeMs = 500;
            ModParticleUtil.playClient(sectorId, pos, EFXSprites.FLASH.getSprite(), particle);

            new StarRunnable() {
                @Override
                public void run() {
                    playBigSmoke(sectorId, pos);
                }
            }.runLater(EFXLite.inst, 7);
            // Orange flash
            ColorFlashParticle cfp = new ColorFlashParticle(25, new Vector4f(1, 1, 0, 1), new Vector4f(1F, 0, 0, 1F));
            cfp.lifetimeMs = 180;
            ModParticleUtil.playClient(sectorId, pos, EFXSprites.FLASH.getSprite(), cfp);

            //30, 1.6
            for (int i = 0; i < 170; i++) {
                InvisibleEmitterParticle iep = new InvisibleEmitterParticle(EFXSprites.FIREFLASH2.getSprite());
                iep.velocity.set(ModParticleUtil.getRandomDir(2.1F));
                iep.lifetimeMs = 2000 + TextureSheetMergeAlgorithm.randInt(500);
                iep.colorA = 0;
                ModParticleUtil.playClient(sectorId, pos, EFXSprites.NOTHING2.getSprite(), iep);
            }

        }else{
            markForDelete();
        }
    }
    public static void playBigSmoke(int sectorId, Vector3f loc){
        for (int i = 0; i < 60; i++) {
            FadeScaleParticle part = new FadeScaleParticle(120, 250 + TextureSheetMergeAlgorithm.randInt(30), 3000);
            part.velocity = ModParticleUtil.getRandomDir(0.7F);
            part.lifetimeMs = 6000;
            ModParticleUtil.playClient(sectorId, loc, EFXSprites.BIGSMOKE.getSprite(), part);
        }
    }
}
