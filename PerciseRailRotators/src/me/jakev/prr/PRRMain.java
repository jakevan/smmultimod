package me.jakev.prr;

import api.config.BlockConfig;
import api.mod.StarMod;
import api.utils.textures.StarLoaderTexture;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.schine.resource.ResourceLoader;

import java.awt.image.BufferedImage;
import java.util.Arrays;

/**
 * Created by Jake on 2021-11-01
 */

public class PRRMain extends StarMod {
    public static ElementInformation pRailCW;
    public static ElementInformation pRailCCW;
    @Override
    public void onBlockConfigLoad(BlockConfig config) {
        short[] railTex = ElementKeyMap.getInfo(ElementKeyMap.RAIL_BLOCK_CW).getTextureIds();
        short[] cwTex = Arrays.copyOf(railTex, railTex.length);
        short[] ccwTex = Arrays.copyOf(railTex, railTex.length);

        cwTex[0] = (short) cwTexture;
        ccwTex[0] = (short) ccwTexture;

        pRailCW = BlockConfig.newElement(this, "Precision Clockwise Rotator Rail", cwTex);
        BlockConfig.setBasicInfo(pRailCW, "block", 100, 0.1F, true, false, cwIcon, 1, 50);

        pRailCCW = BlockConfig.newElement(this, "Precision Counter-Clockwise Rotator Rail", ccwTex);
        BlockConfig.setBasicInfo(pRailCCW, "block", 100, 0.1F, true, false, ccwIcon, 1, 50);

        BlockConfig.addCustomRail(pRailCW, BlockConfig.RailType.ROTATOR);
        BlockConfig.addCustomRail(pRailCCW, BlockConfig.RailType.ROTATOR);

        //Included later
        pRailCW.setBlockStyle(BlockStyle.NORMAL24.id);
        pRailCCW.setBlockStyle(BlockStyle.NORMAL24.id);

        pRailCW.rotatesCW = true;
        pRailCCW.rotatesCW = false;

        BlockConfig.add(pRailCW);
        BlockConfig.add(pRailCCW);

        BlockConfig.addRecipe(pRailCW, 4, 5, new FactoryResource(10, (short) 146));
        BlockConfig.addRecipe(pRailCCW, 4, 5, new FactoryResource(10, (short) 146));
    }
    public static int cwIcon;
    public static int ccwIcon;

    public static int cwTexture;
    public static int ccwTexture;

    @Override
    public void onResourceLoad(ResourceLoader loader) {
        BufferedImage icons = getJarBufferedImage("me/jakev/prr/icons.png");
        cwIcon = StarLoaderTexture.newIconTexture(icons.getSubimage(0,0,64,64)).getTextureId();
        ccwIcon = StarLoaderTexture.newIconTexture(icons.getSubimage(64,0,64,64)).getTextureId();

        BufferedImage textures = getJarBufferedImage("me/jakev/prr/textures.png");
        cwTexture = StarLoaderTexture.newBlockTexture(textures.getSubimage(0,0,256,256)).getTextureId();
        ccwTexture = StarLoaderTexture.newBlockTexture(textures.getSubimage(256,0,256,256)).getTextureId();


    }
}
