package me.jakev.eelite.gui;

import org.schema.game.client.view.gui.shiphud.newhud.Hud;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

/**
 * Created by Jake on 1/31/2022.
 * <insert description here>
 */
public class EFXStatusBar extends GUIElement {
    private final Hud hud;
    GUITextOverlay text;
    public EFXStatusBar(Hud hud, InputState state) {
        super(state);
        this.hud = hud;
    }

    @Override
    public float getWidth() {
        return 100;
    }

    @Override
    public float getHeight() {
        return 100;
    }

    @Override
    public void cleanUp() {

    }

    @Override
    public void draw() {
        text.setPos(300,10,1);
        text.draw();
    }

    @Override
    public void onInit() {
        text = new GUITextOverlay(30,30, FontLibrary.getBlenderProHeavy20(), getState());
        text.setPos(100,100,1);
        text.setTextSimple("penis");
    }
}
