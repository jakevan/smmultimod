package me.jakev.eelite.net;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.particle.ModParticleUtil;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.particles.FlashParticle;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import java.io.IOException;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class PacketSCPlayExplosionParticle extends Packet {
    Vector3f location;
    int sectorId;

    public PacketSCPlayExplosionParticle() {
    }

    public PacketSCPlayExplosionParticle(Vector3f location, int sectorId) {
        this.location = location;
        this.sectorId = sectorId;
    }

    @Override
    public void readPacketData(PacketReadBuffer buf) throws IOException {
        location = buf.readVector3f();
        sectorId = buf.readInt();
    }

    @Override
    public void writePacketData(PacketWriteBuffer buf) throws IOException {
        buf.writeVector3f(location);
        buf.writeInt(sectorId);
    }

    @Override
    public void processPacketOnClient() {
        FlashParticle particle = new FlashParticle(4);
        ModParticleUtil.playClient(sectorId, location, EFXSprites.FLASH.getSprite(), particle);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        // N/A
    }
}
