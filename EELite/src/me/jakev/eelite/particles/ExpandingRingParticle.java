package me.jakev.eelite.particles;

import api.utils.particle.ModParticle;

/**
 * Created by Jake on 7/18/2021.
 * <insert description here>
 */
public class ExpandingRingParticle extends ModParticle {
    private final float startSize;
    private final float endSize;

    public ExpandingRingParticle(float startSize,
                                 float endSize) {
        this.startSize = startSize;
        this.endSize = endSize;
    }

    @Override
    public void update(long currentTime) {
        sizeOverTime(this, currentTime, startSize, endSize);
        fadeOverTime(this, currentTime);
    }
}
